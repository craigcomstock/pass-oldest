# pass-oldest

GNU pass extension to show oldest password.

Install with `sudo make install`. Adjust install dir as needed for different distributions.

  This Makefile works with PureOS aka Debian 10.
  
Use with `pass oldest` to see oldest password that you might want to update.

```
$ pass oldest
2017-12-14+15:47:15.1159113710 email/northern.tech
```
