PREFIX ?= /usr
EXT_DIR=$(PREFIX)/lib/password-store/extensions

install:
	mkdir -p "$(EXT_DIR)"
	cp oldest.bash "$(EXT_DIR)"
