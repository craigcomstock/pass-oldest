(
  DIR=${PASSWORD_STORE_DIR:-~/.password-store}
  cd $DIR;
  find -type f -name '*.gpg' -printf '%T+ %p\n' | sort | head -n 1 | sed -e 's/\.\///' -e 's/\.gpg$//'
)

